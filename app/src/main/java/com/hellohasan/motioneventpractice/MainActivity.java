package com.hellohasan.motioneventpractice;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.ActionBar;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.logging.Logger;

public class MainActivity extends AppCompatActivity {

    ImageView imageView;
    TranslateAnimation animation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.imageView);





    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        float sourceX = imageView.getX();
        float sourceY = imageView.getY();

        float destX = event.getX();
        float destY = event.getY();

        ObjectAnimator animX = ObjectAnimator.ofFloat(imageView, "x", destX-45);
        ObjectAnimator animY = ObjectAnimator.ofFloat(imageView, "y", destY-90);
        AnimatorSet animSetXY = new AnimatorSet();
        animSetXY.setDuration(700);
        animSetXY.playTogether(animX, animY);
        animSetXY.start();

        Log.d("Before (X, y): (", String.valueOf(sourceX) + ", " + String.valueOf(sourceY) +")");
        Log.d("After (X, y): (", String.valueOf(destX) + ", " + String.valueOf(destY) +")");


//        animation = new TranslateAnimation(sourceX,  destX, sourceY,  destY);
//        animation.setDuration(1000);
//        animation.setFillAfter(true);
//        animation.setAnimationListener(new MyAnimationListener());

//        imageView.setX(destX);
//        imageView.setY(destY);

//        imageView.startAnimation(animation);

        return super.onTouchEvent(event);
    }

    private class MyAnimationListener implements Animation.AnimationListener {

        @Override
        public void onAnimationEnd(Animation animation) {
//            imageView.clearAnimation();
//            ConstraintLayout.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(imageView.getWidth(), imageView.getHeight());
//            layoutParams.setMargins(50, 100, 0, 0);
//            imageView.setLayoutParams(layoutParams);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationStart(Animation animation) {
        }

    }
}
